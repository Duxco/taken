var api = require("./api"),
	config = require("./config"),
	express = require("express"),
	www = require("./www");

var app = express();

app.use(api);
app.use(www);

var server = app.listen(config.port, function(){
	console.log('App stared on port ' + config.port);
});
