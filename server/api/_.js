module.exports = function( router, databases, auth ){
	var controller = require("../controllers/controlador")(databases.taken); // <- Change the name of database

	// Get
	router.get("/controladores", auth.api, controller.todos);
	router.get("/controladores/:id", auth.api, controller.porID);
	// Post
	router.post("/controladores", auth.api, controller.agregar);
	// Put (Update)
	router.put("/controladores", auth.api, controller.actualizar);
	// Delete
	router.delete("/controladores/:id", auth.api, controller.eliminar);

	return router;
};