module.exports = function(){
	var mongoose = require("mongoose");

	var createConnection = function( database ){
		var connection;
		switch( database.provider ){
			case "MongoDB":
				var connectionString = "mongodb://" +database.server +"/" +database.name;
				connection = mongoose.createConnection(connectionString);
				connection.on('error', function( error ){
					console.log(error.message);
				});
				connection.on('open', function( error ){
					console.log('Connected to database: ' +database.name +'@' +database.server);
				});
				break;
		}
		return connection;
	};

	return {
		taken: createConnection( require("./taken.json") ) // <- Change the name of database
	};
};